### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 1ef9a962-c820-4fc2-8f20-4af9f03bc7fa
using Pkg

# ╔═╡ 9c66d847-d902-47b0-995f-b1ea8d8b9db3
begin
Pkg.add("PyCall")
Pkg.add("Images")
end

# ╔═╡ 28855198-f2da-458b-817f-9f7d79a732fd
	using PlutoUI

# ╔═╡ eb0ef6ce-7a00-44ae-9c38-056b936e6b5e
using Flux, Flux.Data.MNIST, Statistics

# ╔═╡ 7965cb60-cf5b-11eb-32c3-53d64bc9c01a
using Flux: onehotbatch, onecold, crossentropy, throttle

# ╔═╡ 8e06a942-4b4a-423f-b520-01ae8c984896
using Base.Iterators: repeated, partition

# ╔═╡ 34787ef7-ae65-470b-b57b-f564681af81d
	using Printf, BSON

# ╔═╡ 869af01b-5ad8-4ed8-9395-ce3dbdf17aee
using Distributions

# ╔═╡ a91a083e-911a-4324-8d2d-e910644348d2
using DataFrames

# ╔═╡ 08c2136e-0aab-4913-9b5c-600abbcd3b5b
using PyCall

# ╔═╡ 819f94a7-6071-4ee7-b552-8fd9c227fe66
using Images

# ╔═╡ 302943e7-0dd0-4ef4-92ae-c5d87b304151
using Colors

# ╔═╡ d05fed42-a480-4ccf-8b38-20a1c2bfd547
using HDF5

# ╔═╡ f154fd66-13ce-4739-924c-660a0deac304
using CSV

# ╔═╡ 0a2ec966-c947-4a36-a79c-18682a9d1fb5


# ╔═╡ 1ae8196d-f0a6-4588-b371-9311a8a8bc6f


# ╔═╡ 5fd0a946-67e9-44e9-9e9d-43f8037bef33
Pkg.build("PyCall")

# ╔═╡ cb7af1f3-19b1-4efb-a0e0-6953423bed59
Pkg.build("Images")

# ╔═╡ 344b747a-0855-4770-a05c-7cefb757c764
begin
	trainP = "C:\\Users\\Coolkid_specials\\Desktop\\New folder\\chest_xray\\train"
	testP = "C:\\Users\\Coolkid_specials\\Desktop\\New folder\\chest_xray\\test"
	valP = "C:\\Users\\Coolkid_specials\\Desktop\\New folder\\chest_xray\\val"
end

# ╔═╡ ba5ea5af-af25-4501-8676-214c332c4581
begin
	length(valP)
	length(testP)
	length(trainP)
end

# ╔═╡ 585421c5-2704-4720-b51d-47b44aa2c14c
function minibatch(X, Y, idxs)
    X_batch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    Y_batch = onehotbatch(Y[idxs], 0:9)
    return (X_batch, Y_batch)
end

# ╔═╡ fc38ec37-8738-493c-82fd-a11af5d11809
begin
	data = Iterators.repeated((trainP, testP), 100)
	
	model = Chain(
	  Dense(64, 64, trainP),
	  Dense(64, 32, trainP),
	  Dense(32, 1))
	
	loss(x, y) = Flux.mse(model(x), y)
	ps = Flux.params(model)
	opt = ADAM() 
	evalcb = () -> @show(loss(trainP, testP))
	
	Flux.train!(loss, params(model), data, opt, cb = evalcb)
end

# ╔═╡ 0b73b6ca-df5e-4aeb-8683-d7d3f15abe76
begin
	function readimages(nimages)
	    img=load("/home/miracle/Desktop/chest_xray/val")
	    nx,ny=size(img)
	    @show nx,ny,nimages
	    images=zeros(RGB{Normed{UInt8,8}},nx,ny,nimages)
	    for i=1:nimages
	        img=load("C:\\Users\\Coolkid_specials\\Desktop\\New folder\\chest_xray\\val")
	        images[:,:,i].=img[:,:]
	    end
	    images
	end
	show
end

# ╔═╡ be0648b1-4f28-4ccc-87e8-d653229f35c7

begin

	images = Flux.Data.MNIST.images();
	labels = Flux.Data.MNIST.labels();
end

# ╔═╡ 30c89da5-3a4b-43be-8930-46932c0f323b
begin
	using Random
	x=rand(1:60000)
	md"""
	$(images[x])
	# $(labels[x])"""
end

# ╔═╡ d6ea4c58-0444-4ae5-a378-40700e55fd35
function modelLenet5(; imgsize=(28,28,1), nclasses=2) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
            Conv((5, 5), imgsize[end]=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            flatten,
            Dense(prod(out_conv_size), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, nclasses)
          )
end

# ╔═╡ 114bd87b-7845-4752-9afa-155693731744
function get_data(args)
    xtrain, ytrain = MLDatasets.MNIST.traindata(Float32)
    xtest, ytest = MLDatasets.MNIST.testdata(Float32)

    xtrain = reshape(xtrain, 28, 28, 16, :)
    xtest = reshape(xtest, 28, 28, 16, :)

    ytrain, ytest = onehotbatch(ytrain, 0:9), onehotbatch(ytest, 0:9)

    train_loader = DataLoader((xtrain, ytrain), batchsize=args.batchsize, shuffle=true)
    test_loader = DataLoader((xtest, ytest),  batchsize=args.batchsize)
    
    return train_loader, test_loader
end


# ╔═╡ 9e79e534-a205-4be3-a30f-d3bc3c5f5f9c
begin
num_params(model) = sum(length, Flux.params(model)) 
round4(x) = round(x, digits=4)
end

# ╔═╡ c80c8b8c-8c71-4eec-a6b9-1d99e01943cb
function eval_loss_accuracy(loader, model, device)
    l = 0f0
    acc = 0
    ntot = 0
    for (x, y) in loader
        x, y = x |> device, y |> device
        ŷ = model(x)
        l += loss(ŷ, y) * size(x)[end]        
        acc += sum(onecold(ŷ |> cpu) .== onecold(y |> cpu))
        ntot += size(x)[end]
    end
    return (loss = l/ntot |> round4, acc = acc/ntot*100 |> round4)
end

# ╔═╡ 610dc902-5ca8-4f96-9785-838d71b05fbc


# ╔═╡ 39823adb-88eb-45dd-abf9-3f587b67ac25


# ╔═╡ 4f2deab5-5585-4265-985b-2fdc0dcd0022


# ╔═╡ ce572fd4-7109-4674-b5fa-aa11a9e1ff7d


# ╔═╡ Cell order:
# ╠═1ef9a962-c820-4fc2-8f20-4af9f03bc7fa
# ╠═0a2ec966-c947-4a36-a79c-18682a9d1fb5
# ╠═1ae8196d-f0a6-4588-b371-9311a8a8bc6f
# ╠═28855198-f2da-458b-817f-9f7d79a732fd
# ╠═eb0ef6ce-7a00-44ae-9c38-056b936e6b5e
# ╠═7965cb60-cf5b-11eb-32c3-53d64bc9c01a
# ╠═8e06a942-4b4a-423f-b520-01ae8c984896
# ╠═34787ef7-ae65-470b-b57b-f564681af81d
# ╠═869af01b-5ad8-4ed8-9395-ce3dbdf17aee
# ╠═a91a083e-911a-4324-8d2d-e910644348d2
# ╠═9c66d847-d902-47b0-995f-b1ea8d8b9db3
# ╠═5fd0a946-67e9-44e9-9e9d-43f8037bef33
# ╠═08c2136e-0aab-4913-9b5c-600abbcd3b5b
# ╠═cb7af1f3-19b1-4efb-a0e0-6953423bed59
# ╠═819f94a7-6071-4ee7-b552-8fd9c227fe66
# ╠═302943e7-0dd0-4ef4-92ae-c5d87b304151
# ╠═d05fed42-a480-4ccf-8b38-20a1c2bfd547
# ╠═f154fd66-13ce-4739-924c-660a0deac304
# ╠═344b747a-0855-4770-a05c-7cefb757c764
# ╠═ba5ea5af-af25-4501-8676-214c332c4581
# ╠═585421c5-2704-4720-b51d-47b44aa2c14c
# ╠═fc38ec37-8738-493c-82fd-a11af5d11809
# ╠═0b73b6ca-df5e-4aeb-8683-d7d3f15abe76
# ╠═be0648b1-4f28-4ccc-87e8-d653229f35c7
# ╠═30c89da5-3a4b-43be-8930-46932c0f323b
# ╠═d6ea4c58-0444-4ae5-a378-40700e55fd35
# ╠═114bd87b-7845-4752-9afa-155693731744
# ╠═c80c8b8c-8c71-4eec-a6b9-1d99e01943cb
# ╠═9e79e534-a205-4be3-a30f-d3bc3c5f5f9c
# ╠═610dc902-5ca8-4f96-9785-838d71b05fbc
# ╠═39823adb-88eb-45dd-abf9-3f587b67ac25
# ╠═4f2deab5-5585-4265-985b-2fdc0dcd0022
# ╠═ce572fd4-7109-4674-b5fa-aa11a9e1ff7d
